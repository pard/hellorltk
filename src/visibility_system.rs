extern crate rltk;
use rltk::{field_of_view, Point};
extern crate specs;
use specs::prelude::*;
use super::{Map, Player, Position, Viewshed};

pub struct VisibilitySystem {}

impl<'a> System<'a> for VisibilitySystem {
    type SystemData = ( WriteExpect<'a, Map>,
                        Entities<'a>,
                        ReadStorage<'a, Player>,
                        WriteStorage<'a, Position>,
                        WriteStorage<'a, Viewshed>);

    fn run(&mut self, data: Self::SystemData) {
        let (mut map, entities, player, pos, mut viewshed) = data;

        for (ent, pos, viewshed) in (&entities, &pos, &mut viewshed).join() {
            if viewshed.dirty {
                viewshed.dirty = false;
                viewshed.visible_tiles.clear();
                viewshed.visible_tiles = field_of_view(Point::new(pos.x, pos.y),
                                                        viewshed.range, &*map);
                viewshed.visible_tiles.retain(|p|
                    p.x > 0 && p.x < map.width-1 && p.y > 0 && p.y < map.height-1);

                let p: Option<&Player> = player.get(ent);
                if let Some(_p) = p {
                    for t in map.visible_tiles.iter_mut() { *t = false };
                    for vis in viewshed.visible_tiles.iter() {
                        let idx = map.xy_idx(vis.x, vis.y);
                        map.revealed_tiles[idx] = true;
                        map.visible_tiles[idx] = true;
                    }
                }
            }
        }
    }
}

